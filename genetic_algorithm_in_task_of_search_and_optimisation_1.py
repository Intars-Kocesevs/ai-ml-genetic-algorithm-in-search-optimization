# -*- coding: utf-8 -*-
"""Intars - training file --Genetic Algorithm (demo of its use)

Example of usage of genetic algorithm in search/optimisation

Demo-aim is for genetic algorithm to 'gravitate' towards/to evolve (without any access
and built-in mathematical optimisation formulas) a near-optimal solution for a task of
simplified multi-criterial optimisation challenge:  to find possibly best way how to
load cargo track with various commercial products in order to a) maximally utilize cargo track
conteiner size while simultaneously b) maximally factoring in a potential profit from various
product selling prices.
#"""

message_demo_title = "Example of usage of genetic algorithm in search/optimisation"
print("")
message_about_demo = """Demo-aim is for genetic algorithm to 'gravitate' towards/to evolve (without any access
and built-in mathematical optimisation formulas) a near-optimal solution for a task of
simplified multi-criterial optimisation challenge:  to find possibly best way how to
load cargo track with various commercial products in order to a) maximally utilize cargo track
conteiner size while simultaneously b) maximally factoring in a potential profit from various
product selling prices."""
print(message_about_demo)
print("")
print("")
print("type of product | its size (in m^3) |  price for sell   "), print("")


"""# Product class"""

class Product():
  def __init__(self, product_name, product_space, product_price):
    self.name = product_name
    self.space = product_space
    self.price = product_price



# now we create a list of products with their according properties
products_list = []
products_list.append (Product("Refrigerator A", 0.751, 999.90))
products_list.append (Product("Cell phone", 0.0000899, 2199.12))
products_list.append (Product("TV 55'", 0.400, 4346.99))
products_list.append (Product("TV 50'", 0.290, 3999.90))
products_list.append (Product("TV 42'", 0.200, 2999.90))
products_list.append (Product("Notebook A", 0.00350, 2499.90))
products_list.append (Product("Ventilator", 0.496, 199.90))
products_list.append (Product("Microwave A", 0.0424, 308.66))
products_list.append (Product("Microwave B", 0.0544, 429.90))
products_list.append (Product("Microwave C", 0.0319, 299.29))
products_list.append (Product("Refrigerator B", 0.635, 849.00))
products_list.append (Product("Refrigerator C", 0.870, 1199.89))
products_list.append (Product("Notebook B", 0.498, 1999.90))
products_list.append (Product("Notebook C", 0.527, 3999.00))

# routine to output whole list with all properties
for product in products_list:
  print(product.name, '---', product.space, '---', product.price)

"""# Individual class"""

from random import random

class Individual():
  def __init__(self, spaces, prices, cargo_truck_space_limit, generation = 0):
    # individual solution variant (chromosome) for a given problem will
    # receive properties
    self.spaces = spaces
    self.prices = prices
    self.space_limit = cargo_truck_space_limit
    self.individuals_evaluated_score = 0
    self.individuals_used_space = 0
    self.generation = generation
    # chromosome is defined as list of 0 and 1; below we prepare structure
	# 1 means respective (by its index place in the list) product's inclusion into solution;
	# 0 means its absence;
    self.chromosome = []

    # now, by utilizing random number generator, chromosome structure is
    # initialized with 0 and 1 according to equal probability of 50%
    for i in range(len(spaces)):
      if random() < 0.5:
        self.chromosome.append('0')
      else:
        self.chromosome.append('1')

  # also, an individual needs to be measured for achievement;
  # individual contains fitness function that will do this measurement;
  def fitness(self):
    score = 0             # initialization
    sum_of_used_spaces = 0  # initialization
    for i in range(len(self.chromosome)):
      if self.chromosome[i] == '1':
        score += self.prices[i]
        sum_of_used_spaces += self.spaces[i]
     
    if sum_of_used_spaces > self.space_limit:
      # assignement of very low score to discourage such chromosome
      score = 1

    self.individuals_evaluated_score = score
    self.individuals_used_space = sum_of_used_spaces   

  # genetic crossover operator function
  def crossover(self, other_individual):
    cutoff_point = round(random() * len(self.chromosome))
    # !! INFO ---   uncomment lower print op. for detailed view during GA run   --- !!
    #print('Crossover cutoff point: ', cutoff_point)
    # create first offspring entity by mixing genes from two individuals;
    # '::' will take genes from specified cutoff point to the end of chromosome
    child_1 = self.chromosome[0:cutoff_point] + other_individual.chromosome[cutoff_point::]
    child_2 = other_individual.chromosome[0:cutoff_point] + self.chromosome[cutoff_point::]
    # !! INFO ---   uncomment lower print op. for detailed view during GA run   --- !!
    #print(child_1)
    #print(child_2)
    
    # now we create new individual entities from two crossover chromosomes
    childrens = [Individual(self.spaces, self.prices, self.space_limit, self.generation + 1),
                 Individual(self.spaces, self.prices, self.space_limit, self.generation + 1)]
    # assign respective chromosomes to new entitie chromosome variable
    childrens[0].chromosome = child_1
    childrens[1].chromosome = child_2
    return childrens

  # mutation operator
  def mutation(self, mutation_probability):
    # !! INFO  ----   uncomment lower print op. for detailed view during GA run   -------- !!
    #print("chromosome before mutation: ", self.chromosome)
    for i in range(len(self.chromosome)):
      if random() < mutation_probability:
        if self.chromosome[i] == '1':
          # chromosome's cell then mutates from 1 to 0
          self.chromosome[i] = '0'
        else:
          self.chromosome[i] = '1'  # otherwise it mutates from 0 to 1

    # !! INFO  ----   uncomment lower print op. for detailed view during GA run   -------- !!
    #print("chromosome after  mutation: ", self.chromosome)
    # now changed chromosome object is being returned
    return self


print("")
wait = input("Press Enter to continue.")	# setup for some pause for user to read task and data table
print("Genetic algorithm will start its processes")

"""#   variable/main primitives  testing run   """
print("")
print("----------   diagnostics / test run of important variables and primitives  -----------")
# for purposes of testing, the structures are created and initialized;
# each list structure will hold according information type about products
spaces = []
prices = []
names = []
for product in products_list:
  spaces.append(product.space)
  prices.append(product.price)
  names.append(product.name)
cargo_truck_space_limit = 3   # 3 m^3 is defined limit in this task and considered as a given fact


print(spaces)
print(prices)
print(names)
print("")

individual_1 = Individual(spaces, prices, cargo_truck_space_limit)
print(individual_1.chromosome)
# 0 - product won't be used in a solution variant
# 1 - product will be used in a solution variant;
# for readability 1 is converted into product name and outputed:
for i in range(len(products_list)):
  if individual_1.chromosome[i] == '1':
    print('Name of product:  ', products_list[i].name)

# testing the fitness function
individual_1.fitness()
print('Score (forecasted money units):  ', individual_1.individuals_evaluated_score)
print('Used total space in truck:    ', individual_1.individuals_used_space)
print("")

individual_2 = Individual(spaces, prices, cargo_truck_space_limit)
print(individual_2.chromosome)
# 0 - product won't be used in a solution variant
# 1 - product will be used in a solution variant;
# for readability 1 is converted into product name and outputed:
for i in range(len(products_list)):
  if individual_2.chromosome[i] == '1':
    print('Name of product:  ', products_list[i].name)

# testing the fitness function
individual_2.fitness()
print('Score (forecasted money units):  ', individual_2.individuals_evaluated_score)
print('Used total space in truck:    ', individual_2.individuals_used_space)
print("")

children_entities = individual_1.crossover(individual_2)
children_entities[0].fitness()
print('offspring entity #1 score and chromosome:')
print(children_entities[0].individuals_evaluated_score)
print(children_entities[0].chromosome)

children_entities[1].fitness()
print('offspring entity #2 score and chromosome:')
print(children_entities[1].individuals_evaluated_score)
print(children_entities[1].chromosome)

individual_1.mutation(0.3)

"""# Genetic algorithm class"""

class Genetic_algorithm():
  # first, upon constructor call, object structure will be initialized
  def __init__(self, population_size):
    # initialize default values for all parameters
    self.population_size = population_size
    self.population = []  # will hold individuals here
    self.generation = 0
    self.best_solution = None   # initial value which will change in process
    self.list_of_solutions = []   # potential candidate list for selection

  # first procedure of whole GA--population initialization;
  # population is being built with Individual objects
  def initialize_population(self, spaces, prices, space_limit):
    # filling population variable with array/list of individual's objects
    for i in range(self.population_size):
      self.population.append( Individual(spaces, prices, space_limit) )

    # initial pick of best solution candidate to kick-start selection process
    self.best_solution = self.population[0]

  # function for fitness results ordering/sorting
  def order_population(self):
    # will sort, based on values of populations parameter: individuals_evaluated_score
    self.population = sorted(self.population, key = lambda population: population.individuals_evaluated_score, reverse = True)

  # function recognizes and saves best-scored individual and reuploads it into
  # genetic algorithms whole class specific object - best_solution
  def best_individual(self, sent_in_individual):
    # sent into a function an individual's object parameter of score is being
    # checked against best_solution object's parameter of score points
    if (sent_in_individual.individuals_evaluated_score > self.best_solution.individuals_evaluated_score):
      self.best_solution = sent_in_individual

  # function that checks whole population's achieved total score
  def sum_evaluations(self):
    sum = 0
    for individual in self.population:
      sum += individual.individuals_evaluated_score
    return sum

  # function for selection of parent for next crossover procedure
  def select_parent(self, total_of_population_evaluation):
    parent = -1   # initial value which will be updated as process go
    # below, a random value in range of total popul. evaluation is being created
    # for roulette-selection process
    random_value = random() * total_of_population_evaluation
    selection_threshold = 0
    i = 0
    # !! INFO  ----   uncomment lower print op. for detailed view during GA run   -------- !!
    #print ('\n---  selection process:  generating a random score-cutoff threshold ---', random_value)
    while i < len(self.population) and selection_threshold < random_value:
      # !! INFO  ----   uncomment lower print op. for detailed view during GA run   -------- !!
      #print ('i: ', i, ' | selection_threshold |', selection_threshold)
      selection_threshold += self.population[i].individuals_evaluated_score
      parent += 1   # increment index to cycle through individuals
      i += 1

    return parent

  # function to show next population and some results
  def visualize_generation (self):
    best = self.population[0]   # best individual from sorted population
    print ('Generation: ', self.population[0].generation,
           ' |total price: ', best.individuals_evaluated_score,
           ' |space: ', best.individuals_used_space,
           ' |chromosome: ', best.chromosome)

  # -------   < GA in one function call >   -----------------------------------------------  
  # function to enable full-scope application of this genetic algorithm
  def solve(self, mutation_probability, number_of_generations, spaces, prices, space_limit):
    # [1] -- initialize a population to start GA procedure
    self.initialize_population(spaces, prices, space_limit)

    # [2] -- evaluate population
    for individual in self.population:
      individual.fitness()    # each individual gets evaluated
    # sort the evaluated scores as a preparation step for selection
    self.order_population()

    # now starting to update a list of in each generation found solutions scored value;
    # this list is intended to be output as a graph later on
    self.best_solution = self.population[0]
    self.list_of_solutions.append(self.best_solution.individuals_evaluated_score)

    # after sorting, print best individual from current gen.population
    self.visualize_generation()

    # [3] -- GA stopping criterion / setup of GA run
    for generation in range (number_of_generations):
      # [4] -- selection process of parent-entities for creation of children-entities
      sum = self.sum_evaluations()
      new_population = []   # initialize as a placeholder for updates during process

      for new_individual in range(0, self.population_size, 2):
        # select parent-entities
        parent1 = self.select_parent(sum)
        parent2 = self.select_parent(sum)
        # [5] crossover genetic operator
        children_entities = self.population[parent1].crossover(self.population[parent2])
        # [6] mutation genetic operator; after it new population is being started to be formed
        new_population.append(children_entities[0].mutation(mutation_probability))
        new_population.append(children_entities[1].mutation(mutation_probability))
        # after last loop cycle population will be formed;
        # old/previous population now will be discarded and replaced with newly made population

      self.population = list(new_population)

      # [7] now new population must be evaluated and whole GA cycle repeated
      for individual in self.population:
        individual.fitness()
      
      self.visualize_generation()
      best = self.population[0]   # initial pick

      # now is time to update so far found best solutions scored value list array
      self.list_of_solutions.append(best.individuals_evaluated_score)

      # also initialize this object so it starts participating all throughout GA
      self.best_individual(best)

    # ----- GA main loop ends here -----
    #print iteration's result
    print ('Evolved solution:')
    print ('**** Best solution - generation: ', self.best_solution.generation,
           ' |total price: ', self.best_solution.individuals_evaluated_score,
           ' |space: ', self.best_solution.individuals_used_space,
           ' |chromosome: ', self.best_solution.chromosome)
    return self.best_solution.chromosome

popul_size = 15
ga = Genetic_algorithm(popul_size)
ga.initialize_population(spaces, prices, cargo_truck_space_limit)

ga.population

# one individual (represented as chromosome) from population
ga.population[0].chromosome

for individual in ga.population:
  individual.fitness()

# now sorting the result
ga.order_population()
# now printing fitness evaluation results
for i in range(ga.population_size):
  print('individual: ', i, '\nspaces of items: ', ga.population[i].spaces, '\nprices: ', ga.population[i].prices,
        '\nchromosome: ', ga.population[i].chromosome, '\nscore: ', ga.population[i].individuals_evaluated_score, '\n')

# now, after getting first sorted population, best individual is checked against
# initial / previous best individual;
ga.best_individual (ga.population[0])

# for verification, printing currently held best individual
print(ga.best_solution.individuals_evaluated_score)
print(ga.best_solution.chromosome)

# check-up of populations total score
sum = ga.sum_evaluations()
print('Total achieved score throughout iterated population:  ', sum)

# test-demo of parent selection
print ('test-demo of parent selection (for diagnostics purpose)')
parent1 = ga.select_parent(sum)
parent1

# test-demo of parent selection
print ('test-demo of parent selection (for diagnostics purpose)')
parent2 = ga.select_parent(sum)
parent2

# starting a formation of new, next population
new_population = []
mutation_probability = 0.01
# below starts a parenting procedure for entities;
# 2 in function argument list forces each parent entity to be utilized
# in crossover only once; 
for new_individual in range (0, ga.population_size, 2):
  parent1 = ga.select_parent(sum)
  parent2 = ga.select_parent(sum)
  print ('printing entity-parenting scheme:')
  print (parent1, parent2)
  print ('crossover result for children entities:')
  children_entities = ga.population[parent1].crossover(ga.population[parent2])
  print ('printing parent-entities chromosomes:')
  print (ga.population[parent1].chromosome)
  print (ga.population[parent2].chromosome)
  # after everything, mutation is being applied on top of results
  new_population.append (children_entities[0].mutation(mutation_probability))
  new_population.append (children_entities[1].mutation(mutation_probability))

"""# Genetic algorithm in full-run"""

# previous code contains intermidiate tests and check-ups of correct workings of functions;
# below is full GA run for problem solving;
# recreate original data to be worked upon;
print ('************************  main GA run  ******************************')
products_list = []
products_list.append (Product("Refrigerator A", 0.751, 999.90))
products_list.append (Product("Cell phone", 0.0000899, 2199.12))
products_list.append (Product("TV 55'", 0.400, 4346.99))
products_list.append (Product("TV 50'", 0.290, 3999.90))
products_list.append (Product("TV 42'", 0.200, 2999.90))
products_list.append (Product("Notebook A", 0.00350, 2499.90))
products_list.append (Product("Ventilator", 0.496, 199.90))
products_list.append (Product("Microwave A", 0.0424, 308.66))
products_list.append (Product("Microwave B", 0.0544, 429.90))
products_list.append (Product("Microwave C", 0.0319, 299.29))
products_list.append (Product("Refrigerator B", 0.635, 849.00))
products_list.append (Product("Refrigerator C", 0.870, 1199.89))
products_list.append (Product("Notebook B", 0.498, 1999.90))
products_list.append (Product("Notebook C", 0.527, 3999.00))
spaces = []
prices = []
names = []
for product in products_list:
  spaces.append(product.space)
  prices.append(product.price)
  names.append(product.name)
cargo_truck_space_limit = 3   # 3 m3 is defined limit in task and is given
population_size = 20
mutation_probability = 0.01
number_of_generations = 100

# now create GA runned instance
ga = Genetic_algorithm(population_size)
result = ga.solve(mutation_probability, number_of_generations, spaces, prices, cargo_truck_space_limit)
print('\n best solution\'s chromosome outlined below:')
print(result)
# now the final formated, more readable output is being formed

for i in range(len(products_list)):
  if result[i] == '1':
    print('name of product: ', products_list[i].name, ' |price: ', products_list[i].price)

# decommenting for-cycle below allows to print in a column values
# of best solution values during all generations
#for value in ga.list_of_solutions:
#  print(value)

print("")
wait = input("Press Enter to quit.")	# setup for some pause for user to read task and data table
# possible next step could be graph output for solutions values over generations