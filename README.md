## Name
Demo of genetic algorithm

## Description
Genetic algorithms is one of the 'dialect' / methodology in the evolutionary algorithms approach in science of AI/machine learning.
This code showcases a demo usage of basic genetic algorithm in a task of simple multi-criterial optimization need.
It relies on no manually built-in precise mathematical expertise from the problem domain, instead relying on formalized analogies
from nature/genetics and evolution process, imitating those in computer memory.

Demo is founded on task of finding optimal solution to a cargo delivery truck's filling scheme with commercial products under
two criteria constraints: maximally possible usage of cargo container's size and maximum potential profit from cargo item's sale.

![alt text](screenshots_-_genetic_algorithm_for_demo-1.jpg "genetic algorithm demo-1 in action")

## Usage
Open and run downloaded source-code py file in any Python 3.9 supportive environment.

## Roadmap
- Convertation of demo from Python to C++
- Evolving values graph, some UI and informative graphics
